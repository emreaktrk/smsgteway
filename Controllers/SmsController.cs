﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Firebase.Database;
using Firebase.Database.Query;
using Microsoft.AspNetCore.Mvc;
using SmsGateway.Models;

namespace SmsGateway.Controllers
{
    [Route("sms")]
    public class SmsController : Controller
    {
        [Route("send")]
        [HttpPut]
        public async Task<ActionResult> Send([FromBody] Sms sms)
        {
            var exception = sms?.HasException();
            if (exception != null)
            {
                return BadRequest(exception);
            }

            var ulid = await sms.Send();
            return Ok(ulid);
        }

        [Route("code")]
        [HttpPut]
        public async Task<ActionResult> Code([FromBody] Sms sms)
        {
            return await Send(new CodeDecorator(sms));
        }

        [Route("cancel")]
        [HttpDelete]
        public async Task<ActionResult> Cancel([FromBody] Sms sms)
        {
            var exception = sms?.HasException();
            if (exception != null)
            {
                return BadRequest(exception);
            }

            var result = await sms.Cancel();
            if (result > 0)
            {
                return Ok();
            }
            else
            {
                return NotFound("Not found any waiting sms to cancel");
            };
        }
    }
}