using System;
using System.Threading.Tasks;
using Firebase.Database;
using Firebase.Database.Query;
using Newtonsoft.Json;
using NUlid;

namespace SmsGateway.Models
{
    public class Sms : ISms, ISendable, ICancalable
    {
        public string To;
        public string Body;

        public async Task<Ulid> Send()
        {
            var firebase = new FirebaseClient("https://smsgteway.firebaseio.com/");
            var ulid = Ulid.NewUlid();

            await firebase
                    .Child(ulid.ToString())
                    .PutAsync(this);

            firebase.Dispose();

            return ulid;
        }

        public async Task<int> Cancel()
        {
            int result = 0;

            var firebase = new FirebaseClient("https://smsgteway.firebaseio.com/");
            var waitings = await firebase
                                    .Child("smsgteway")
                                    .OnceAsync<Sms>();

            foreach (var sms in waitings)
            {
                if (sms.Object.Equals(this))
                {
                    result = result + 1;

                    await firebase
                            .Child(sms.Key)
                            .DeleteAsync();
                }
            }

            firebase.Dispose();

            return result;
        }

        public static async Task<Boolean> CancelBy(Ulid ulid)
        {
            var firebase = new FirebaseClient("https://smsgteway.firebaseio.com/");
            await firebase
                    .Child(ulid.ToString())
                    .DeleteAsync();

            return true;
        }

        public Exception HasException()
        {
            if (To == null)
            {
                return new NullReferenceException("Sms must have a value");
            }

            if (Body == null)
            {
                return new NullReferenceException("To is null");
            }

            return null;
        }
    }

    public interface ISms
    {
        Exception HasException();
    }

    public interface ISendable
    {
        Task<Ulid> Send();

    }

    public interface ICancalable
    {
        Task<int> Cancel();
    }

}