using System;

namespace SmsGateway.Models
{
    public abstract class Decorator : Sms
    {
        public Decorator(Sms sms)
        {
            To = sms.To;
            Body = sms.Body;
        }
    }

    public class CodeDecorator : Decorator
    {
        public CodeDecorator(Sms sms) : base(sms)
        {
            Body = "Your code is " + sms.Body;
        }
    }

}
